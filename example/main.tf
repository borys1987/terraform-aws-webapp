provider "aws" {
  region = "eu-west-2"
}

module "web-app-instance" {
  source = "./.."
  namespace = "eg"
  name = "web-app"
}
