
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet" "first" {
  availability_zone = data.aws_availability_zones.available.names[0]
  default_for_az    = true
}

data "aws_subnet" "second" {
  availability_zone = data.aws_availability_zones.available.names[1]
  default_for_az    = true
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "aws_security_group" "allow_db_access" {
  name        = "${module.this.id}-db-access"
  description = "Allow access to the database"
}

module "rds_instance" {
  source               = "cloudposse/rds/aws"
  version              = "0.40.0"
  context              = module.this.context
  attributes           = ["rds"]
  security_group_ids   = [aws_security_group.allow_db_access.id]
  database_name        = "webapp"
  database_user        = "webapp"
  database_password    = random_password.password.result
  database_port        = 5432
  storage_type         = "gp2"
  allocated_storage    = 10
  storage_encrypted    = true
  engine               = "postgres"
  engine_version       = "14.5"
  major_engine_version = "14"
  instance_class       = "db.t3.micro"
  db_parameter_group   = "postgres14"
  publicly_accessible  = false
  subnet_ids = [
    data.aws_subnet.first.id,
    data.aws_subnet.second.id
  ]
  vpc_id                      = data.aws_vpc.default.id
  auto_minor_version_upgrade  = true
  allow_major_version_upgrade = false
  apply_immediately           = false
  maintenance_window          = "Mon:03:00-Mon:04:00"
  skip_final_snapshot         = false
  copy_tags_to_snapshot       = true
  backup_retention_period     = 7
  backup_window               = "22:00-03:00"
}

module "instance" {
  source                    = "cloudposse/ec2-instance/aws"
  version                   = "0.45.1"
  attributes                = ["instance"]
  ami                       = data.aws_ami.ubuntu.id
  ami_owner                 = data.aws_ami.ubuntu.owner_id
  instance_type             = "t3.micro"
  vpc_id                    = data.aws_vpc.default.id
  security_groups           = [aws_security_group.allow_db_access.id]
  subnet                    = data.aws_subnet.first.id
  context                   = module.this.context
  ssm_patch_manager_enabled = true
}